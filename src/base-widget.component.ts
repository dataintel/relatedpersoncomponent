import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-relatedperson',
  template: `
  <div class="container" style="padding: 10px">
    <div class="col-md-4">
        <h3 class="red">Related Persons</h3>
        <hr>
        <small *ngFor = "let items of data">{{items.related_persons}}, </small> 
    </div>
</div>
 `,
  styles: [`.red {
    color: red;
    font-weight: bold;
}

.col-md-4 small{
    color: #bbb7b7;
    text-transform: capitalize;
}`]
})

export class BaseWidgetRelatedPersonComponent  {

    public data: Array<any> = [
    {"related_persons":"Afrikaans"},
    {"related_persons":"العربية"},
    {"related_persons":"Беларуская"},
    {"related_persons":"Català"},
    {"related_persons":"Deutsch"},
    {"related_persons":"Español"},
    {"related_persons":"Esperanto"},
    {"related_persons":"Euskara"},
    {"related_persons":"Français"},
    {"related_persons":"हिन्दी"},
    {"related_persons":"Hrvatski"},
    {"related_persons":"Italiano"},
    {"related_persons":"Lietuvių"},
    {"related_persons":"Nederlands"},
    {"related_persons":"日本語"},
    {"related_persons":"Português"},
    {"related_persons":"Română"},
    {"related_persons":"Русский"},
    {"related_persons":"Slovenščina"}];

}
