import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetRelatedPersonComponent } from './src/base-widget.component';

export * from './src/base-widget.component';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetRelatedPersonComponent
  ],
  exports: [
    BaseWidgetRelatedPersonComponent
  ]
})
export class RelatedPersonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RelatedPersonModule,
      providers: []
    };
  }
}
